let mapleader = " "

"Plugged
call plug#begin()
	Plug 'preservim/nerdtree'
	Plug 'gruvbox-community/gruvbox'
call plug#end()

"NERDTree
nnoremap <C-f> :NERDTreeToggle<CR>
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

"Gruvbox
colorscheme gruvbox

"Sets
set number relativenumber
set scrolloff=8
set ruler
set tabstop=4
set shiftwidth=4
set ignorecase
set smartcase
set incsearch
set smartindent
set nowrap
set nohlsearch
set noswapfile

"Mappings
nnoremap <leader>o :setlocal spell! spelllang=en_gb<CR>
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
nnoremap Y y$
nnoremap <leader><leader> /<++><CR>ca<
vnoremap <leader>yy :!xclip -f -sel clip<CR>

"Autocommands
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\n\+\%$//e
autocmd BufWritePre *.[ch] %s/\%$/\r/e
autocmd Filetype rmd map <F5> :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla<enter><enter>
